import React, { Component } from 'react';
import { StyleSheet, Text, View,  ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Tugas12 extends Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.about}>
                    <Text style={styles.tentang}> Tentang Saya </Text>
                    <Icon name="user-circle" style={styles.icontentang} />
                    <Text style={styles.nama}> Yahya Dian P </Text>
                    <Text style={styles.react}> React Native Developer </Text>
                </View>
                <View style={styles.portofolio}>
                    <View style={styles.kotakportofolio}>
                        <Text style={styles.textportofolio}>Portofolio</Text>
                    </View>
                    <View style={styles.konten}>
                        <View>
                            <Icon name="gitlab" style={styles.iconkonten}/>
                            <Text style={styles.textkonten}>@yahyadianp</Text>
                        </View>
                        <View>
                            <Icon name="github" style={styles.iconkonten}/>
                            <Text style={styles.textkonten}>@yahyadianp</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.portofolio}>
                    <View style={styles.kotakportofolio}>
                        <Text style={styles.textportofolio}>Hubungi Saya</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon name="facebook-official" style={styles.iconcontact}/>
                        <Text style={styles.textcontact}>Yahya Dian P</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon name="instagram" style={styles.iconcontact}/>
                        <Text style={styles.textcontact}>@yahyadianp</Text>
                    </View>
                    <View style={styles.row}>
                        <Icon name="twitter" style={styles.iconcontact}/>
                        <Text style={styles.textcontact}>@yahyadianp</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    about: {
        marginTop: 50,
        alignItems: 'center'
    },
    tentang: {
        fontSize: 30,
        color: '#003366',
    },
    icontentang: {
        margin: 10,
        backgroundColor: 'grey',
        borderRadius: 100,
        color: '#EFEFEF',
        fontSize: 200
    },
    nama: {
        fontSize: 20,
        color: '#003366',
    },
    react: {
        fontSize: 15,
        color: '#3EC6FF'
    },
    portofolio: {
        margin: 20,
        borderRadius: 10,
        backgroundColor: '#EFEFEF'
    },
    kotakportofolio: {
        margin: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#003366',
    },
    textportofolio: {
        fontSize: 15,
        color: '#003366',
    },
    konten: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    },
    iconkonten: {
        textAlign: 'center',
        fontSize: 40,
        color: '#3EC6FF'
    },
    textkonten: {
        color: '#003366',
    },
    row: {
        flexDirection: 'row',
        paddingLeft: 90
    },
    iconcontact: {
        padding: 10,
        fontSize: 40,
        color: '#3EC6FF'
    },
    textcontact: {
        textAlignVertical: 'center',
        color: '#003366',
    }
});