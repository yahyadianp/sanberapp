import React from 'react';
import { View, Text, Image, ScrollView, FlatList, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import logo from './images/logo.png'
import data from '../skillData.json'
import ListSkill from './ListSkill'

export default class App extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.loclogo}>
                    <Image source={logo} style={styles.logo} />
                </View>
                <View style={styles.locuser}>
                    <Icon name='user-circle' style={styles.logouser} />
                    <View style={styles.hainama}>
                        <Text style={styles.hai}>Hai,</Text>
                        <Text style={styles.nama}>Mukhlis Hanafi</Text>
                    </View>
                </View>
                <View style={styles.locskill}>
                    <Text style={styles.skill}>SKILL</Text>
                </View>
                <View style={styles.locmenu}>
                    <TouchableOpacity style={styles.kotakmenu}>
                        <Text style={styles.tulisanmenu}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.kotakmenu}>
                        <Text style={styles.tulisanmenu}>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.kotakmenu}>
                        <Text style={styles.tulisanmenu}>Teknologi</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    <FlatList
                        data={data.items}
                        renderItem={(video) => <ListSkill data={video.item} />}
                        keyExtractor={(item) => item.id}
                        ItemSeparatorComponent={() => <View style={{ marginTop: 5 }} />}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10
    },
    loclogo: {
        alignItems: 'flex-end',
    },
    logo: {
        width: 200,
        resizeMode: 'contain'
    },
    locuser: {
        marginTop: -20,
        flexDirection: 'row',
        justifyContent: 'flex-start',
    },
    logouser: {
        fontSize: 40,
        color: '#3EC6FF',
    },
    hainama: {
        flexDirection: 'column',
        justifyContent: 'center',
        marginLeft: 10
    },
    hai: {
        fontSize: 10,
        color: '#003366',
    },
    nama: {
        fontSize: 15,
        color: '#003366',
    },
    locskill: {
        marginTop: 10,
        borderBottomWidth: 3,
        borderBottomColor: '#3EC6FF',
    },
    skill: {
        color: '#003366',
        fontSize: 30
    },
    locmenu: {
        marginTop: 5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10
    },
    kotakmenu: {
        backgroundColor: '#3EC6FF',
        paddingLeft: 5,
        paddingRight: 5,
        height: 25,
        borderRadius: 10,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    tulisanmenu: {
        fontSize: 12,
        color: '#003366',
    },
    body: {
        flex: 1
    },
});