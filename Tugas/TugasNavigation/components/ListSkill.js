import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class ListSkill extends Component {
    render() {
        let data = this.props.data;
        return (
            <View style={styles.container}>
                <View style={styles.loclogo}>
                    <Icon name={data.iconName} style={styles.logoicon} />
                </View>
                <View style={styles.locText}>
                    <View style={styles.locSkill}>
                        <Text style={styles.skillName}>{data.skillName}</Text>
                        <Text style={styles.categoryName}>{data.categoryName}</Text>
                    </View>
                    <View style={styles.locpercent}>
                        <Text style={styles.percentageProgress}>{data.percentageProgress}</Text>
                    </View>
                </View>
                <View style={styles.locarrow}>
                    <Icon name="chevron-right" style={styles.arrow} />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#B4E9FF',
        borderRadius: 20
    },
    loclogo: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
    logoicon: {
        fontSize: 80,
        color: '#003366',
    },
    locText: {
        flexDirection: 'column',
        justifyContent: 'center',
        width: 140,
    },
    locSkill: {
        margin: 0
    },
    skillName: {
        fontSize: 20,
        color: '#003366'
    },
    categoryName: {
        fontSize: 13,
        color: '#3EC6FF'
    },
    locpercent: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    percentageProgress: {
        color: 'white',
        fontSize: 40
    },
    locarrow: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
    arrow: {
        color: '#003366',
        fontSize: 70
    }
});